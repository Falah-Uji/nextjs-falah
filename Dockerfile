FROM node:16-alpine

RUN apk update && \
    apk add --no-cache curl

WORKDIR /app

COPY . .

RUN npm install
RUN npm run build

EXPOSE 3000

CMD ["npm", "start"]
